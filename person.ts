export class Person {
  name: string;
  lastName: string;
  age: number;
  kilometers: number;

  caminar() {
    console.log("estoy caminando.");
  }

  constructor(kilometers: number) {
    this.kilometers = kilometers;
  }
}
